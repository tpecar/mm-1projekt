Glej izvorno kodo za dokumentacijo o dejanskem adaptivnem kompresorju.

Tu je nekaj primerov rezultatov:

Dragic_0.9_0.5_fast.png - 148061 (36.15%), 5.742 s
Dragic_0.9_0.5_slow.png - 86091 (21.02%), 31.372 s
Dragic_0.75_0.125_fast.png - 35538 (8.68%), 6.687 s
Tadej_0.9_0.5_fast.png - 57322 (35.83%), 4.340 s
Tadej_0.9_0.5_slow.png - 39689 (24.81%), 13.809 s
Mondrian_0.9_0.025_slow.png - 8869 (2.50%), 21.084 s

Zakljucxek:
Razlicxno veliki bloki pridejo do izraza sxele, ko hocxemo neko dovolj nizko
razmerje velikosti, drugacxe je dovolj dobro, cxe preprosto shranimo celo
sliko kot en blok. To je seveda odvisno od zxelene kvalitete. Povedano
drugacxe: prednost razlicxno velikih blokov se pokazxe takrat, ko zahtevamo
visoko kvaliteto v majhnem prostoru, cxe imamo sliko, ki ima dele z
razlicxnimi stopnjami podrobnosti; cxe slika npr. vsebuje velik del ene
barve, lahko veliko prihranimo, cxe shranimo le enega ali nekaj blokov zanj,
kot pa cxe bi shranjevali bloke s fiksno velikostjo/gostoto.
(??? Cxe hocxemo kaj pametnega povedati o tem, moramo sxe eksperimentirati.)

Drugacxe se mi zdi v vsakem primeru nujno, da v porocxilu/slide showu
predstavimo ceno shranjevanja U/S/Vt proti originalni sliki. Torej, cxe
ilustriram v ASCII artu:

|||||..... %......... ----------
|||||..... .%........ ----------
|||||..... ..%....... ----------
|||||..... ...%...... ----------
|||||..... ....%..... ----------
|||||..... .......... ..........
|||||..... .......... ..........
|||||..... .......... ..........
|||||..... .......... ..........
|||||..... .......... ..........

   U           S          Vt

Oz. cxe damo vse na kup, da je sxtevilo vrednosti, ki jih je potrebno
shraniti, lazxje primerjati z velikostjo originalne slike:

%++++-----
+%+++-----
++%++-----
+++%+-----
++++%-----
|||||.....
|||||.....
|||||.....
|||||.....
|||||.....
