% Dekompresor adaptivno stisnjenih slik
% MH

function A = Rekonstruiraj(v, debug)
  % Vzame vektor 'v', ki ga vrne funkcija Adapt2, in rekonstruira sliko.
  % Cxe je 'debug' true, na sliko narisxe pravokotnike, ki oznacxujejo meje
  % blokov.

  tic();

  A = zeros(v(1), v(2));
  i = 3;
  while i <= length(v)
    x = v(i);
    y = v(i + 1);
    h = v(i + 2);
    w = v(i + 3);
    ns = v(i + 4);

    U = zeros(h, ns);
    S = zeros(ns, ns);
    Vt = zeros(ns, w);
    i = i + 5;
    for j = 1 : ns
      U(:, j) = v(i : i + h - 1);
      i = i + h;
      S(j, j) = v(i);
      i = i + 1;
      Vt(j, :) = v(i : i + w - 1);
      i = i + w;
    end
    A(y : y + h - 1, x : x + w - 1) = U * S * Vt;

    if debug
      A = Pravokotnik(A, x, y, w, h);
    end
  end

  % Pretvorimo vrednosti v nepredznacxena 8-bitna cela sxtevila.
  A = round(A);
  A(find(A < 0)) = 0;
  A(find(A > 255)) = 255;
  A = uint8(A);

  t = toc();
  printf('%.3f s, %d Hz\n', t, length(v) / t);
end

function A = Pravokotnik(A, x, y, w, h)
  % Narisxe pravokotnik zxelenih dimenzij na matriko A in vrne novo matriko.

  A(y, x : x + w - 1) = 128;
  A(y + h - 1, x : x + w - 1) = 128;
  A(y : y + h - 1, x) = 128;
  A(y : y + h - 1, x + w - 1) = 128;
end