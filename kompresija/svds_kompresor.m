function [U, S, V] = svds_kompresor(M, q)
  M = cast(M,"double");
  
  % sled M'*M, ki je enaka vsoti kvadratov vseh sigularnih vrednosti 
  %  matrike M
  sumAll = trace(M'*M);
 
  sumSelect = 0; % seštevek kvadratov že izbranih singularnh vrednosti 
  
  idx = 1; % indeks v diagonali
  
  while(sumAll==0 && idx == 1 ||  % če je vsota vseh sing. vrednosti enaka 0
  sumAll != 0 &&  (sumSelect/sumAll)<q && idx <= length(M))        
    
    [U, S, V] = svds(M,idx);
    T = S.^2;
    sumSelect = sum(sum(T));
    idx++;      
  end      
  %idx--;
end