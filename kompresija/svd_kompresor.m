function [Uk, Sk, Vk] = svd_kompresor(blok, q)
% kompresira sliko glede na kriterij q, podan v opisu naloge
% (tj. glede na razmerje med vsoto prvih najvecjih ter vseh singularnih
%  vrednosti)
% dobi blok, vrne U, S, V ter stevilo singularnih vrednosti
  [U, S, V] = svd(blok);
  sumAll = sum(sum(S));
  sumSelect = 0;
  
  % vzamemo manjsega izmed robov
  st_singularnih = min(size(S, 1), size(S, 2));
  
  % hranimo le do idx
  idx = 1; % indeks v diagonali
  if(sumAll>0) % lahko se zgodi, da imamo istobarvni blok - v tem potrebujemo
               % le en stolpec/vrednost/vrstico
    while((sumSelect./sumAll)<q && idx <= st_singularnih)
      sumSelect += S(idx,idx);
      idx++;
    end
    if(idx>st_singularnih)
      idx--; % da vrnemo na zadnji veljaven indeks
    end
  end
  % vzamemo le do idx
  Uk = U(:,1:idx); % levi singularni vektorji
  Sk = S(1:idx,1:idx); % singularne vrednosti
  Vk = V(:,1:idx); % desni singularni vektorji
end