function A = svd_dekompresor(Uk, Sk, Vk)
% podane kompresirane U, S, V matrike zmnozimo v priblizek prvotne
% po pravilih moramo kljub manjsim vhodnim matrikam kot zmnozek dobiti matriko
% v velikosti bloka

% v primeru drugih dekompresorjev se lahko izvede prvo se kaksno predprocesiranje
% nad Uk, Sk, Vk, nato pa klice ta dekompresor
  A = Uk*Sk*Vk';
end