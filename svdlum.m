function svdlum()
  A = imread("./slike/d2.jpg");
  B = rgb2ycbcr(A);
  k = 16; % velikost bloka
  q = 0.5; % kvaliteta - 1 najboljše, 0 najslabše
  
  dekompresor = @svd_dekompresor;
  
  Y=B(:,:,1);
  CB=B(:,:,2);
  CR=B(:,:,3);
  
  CB=svdchannel(CB, k, @(CB) svd_kompresor(CB, q), dekompresor);
  CR=svdchannel(CR, k, @(CR) svd_kompresor(CR, q), dekompresor);
  q=1;
  Y=svdchannel(Y, k, @(Y) svd_kompresor(Y, q), dekompresor);
 
  N=Y;
  N(:,:,2)=CB;
  N(:,:,3)=CR;
  N=ycbcr2rgb(N);
  
  imwrite(N,"./slike/d2LUM8.png");
end