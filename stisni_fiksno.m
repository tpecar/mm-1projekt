function izhod=stisni_fiksno(A, rob_bloka, kompresor)
% Izvede kompresijo s fiksnimi bloki, ob upostevanju ostankov na desnem ter
% spodnjem robu.
% - A predstavlja sivinsko sliko - dobimo vrednosti od 0 do 255
% - rob_bloka dolzina roba bloka
% - kompresor je funkcija, ki na podlagi svd razcepa ter nekega kriterija/
%   obdelave podatkov stisne podani blok
% -------------------------------------------------------------------------
% vrne vektor kompresirane slike, ki jo je mogoce razsiriti preko razsiri.m

% primer uporabe
% test = round(rand(5,5)*255)
% t_komp = @(blok) svd_kompresor(blok, 1)
% stisk = stisni_fiksno(neki, 3, t_komp)
% razsiri(stisk, @svd_dekompresor)
%
% Rezultat bi moral biti enak matriki test.

%     KOMPRESOR
%     ta dobi kot vhod trenutni blok (velikost bloka se doloci preko
%     sirine/visine podane matrike)
%
%     kot izhod pa vrne skrajsane U, S, V matrike (potrebni stolpci, diagonalna
%     matrika, potrebne vrstice)

% dobra stran locenega kompresorja je, da lahko analiziramo tudi samo posamezen
% blok

  % izhodni vrsticni vektor, ki nosi podatke o stisnjeni sliki
  % njegov format je
  % [ST_VRSTIC_SLIKE, ST_STOLPCEV_SLIKE,  (to je glava formata)
  % {X_ROBA, Y_ROBA, ST_VRSTIC_BLOKA, ST_STOLPCEV_BLOKA, ST_SINGULARNIH_VRED,
  % {I-TI_STOLPEC_U, I-TA_SINGULARNA_VREDNOST, I-TA_VRSTICA_V}
  %        (ponovi tolikokrat, kolikor je ST_SINGULARNIH_VRED)
  % } (ponovi tolikokrat, kolikor je blokov slike)
  % ]
  % razsiritev s pripadajocim dekompresorjem opravi funkcija razsiri.m
  izhod = [size(A, 1), size(A, 2)];

  zac_vrstica = 1; % zacetna vrstica (zgornjega levega roba)
  vrstic_t_bloka = rob_bloka; % stevilo vrstic trenutnega bloka
  
  while(zac_vrstica <= size(A,1))    
    if(zac_vrstica+rob_bloka-1 > size(A,1))
      vrstic_t_bloka = size(A,1) - zac_vrstica +1;
    endif
    
    zac_stolpec = 1; % zacetni stolpec (zgornjega levega roba)
    stolpcev_t_bloka = rob_bloka; % stevilo stolpcev trenutnega bloka
    
    while(zac_stolpec <= size(A,2))
      % robni primer
      if(zac_stolpec+rob_bloka-1 > size(A,2))
        stolpcev_t_bloka = size(A,2) - zac_stolpec +1;
      endif
      
      % blok ki ga trenutno obdelujemo
      blok = A(zac_vrstica:(zac_vrstica + vrstic_t_bloka-1), zac_stolpec:(zac_stolpec + stolpcev_t_bloka-1));
      
      % poreko kompresorja stisnemo sliko, s cimer pridobimo vektorje
      % stisnjene slike U, S, V, pri cemer stevilo singularnih vrednosti
      % ugotovimo od dimenzije kvadratne matrike S
      
      [U, S, V] = kompresor(blok);
      
      % nastavimo glavo bloka
      izhod_blok = [zac_stolpec, zac_vrstica, vrstic_t_bloka, stolpcev_t_bloka, length(S)];
      
      t_singularna = 1; % trenutno gledana singularna vrednost
      while(t_singularna <= length(S))
        izhod_blok = [izhod_blok, U(:, t_singularna)', ...
                                  S(t_singularna, t_singularna), ...                      
        % mi vzamemo V po stolpcih da lahko stlacimo v format
                                  V'(t_singularna, :)];
        t_singularna = t_singularna + 1;
      end
      % dodamo blok v izhod
      izhod = [izhod, izhod_blok];
      
      zac_stolpec += stolpcev_t_bloka;
    end    
    zac_vrstica += vrstic_t_bloka;
  end
end
%      % vzamemo manjega izmed robov trenutnega bloka
%      rob_t_bloka = min(size(S, 1), size(S, 2));
%      t_singularna = 1; % katero singularno vrednost po vrsti obdelujemo
%      while true
%        if t_singularna == rob_t_bloka || vsota_vseh_S == 0 || vsota_dela_S / vsota_vseh_S >= q
%          break;
%        end