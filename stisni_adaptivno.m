% Adaptivno stiskanje deux
% MH

function v = stisni_adaptivno(A, q, r, hitro)
  % Vstopna tocxka za adaptivni kompresor.
  %
  % 'A' je slika (en kanal); vrednosti morajo biti v intervalu [0, 255].
  % 'q' je razmerje vsot shranjenih singularnih vrednosti proti vsoti vseh za
  % vsak blok v intervalu (0, 1], torej v grobem dolocxa kvaliteto slike.
  % 'r' dolocxa, koliko prostora lahko zasede stisnjena slika v primerjavi z
  % nestisnjeno ([0, 1]). Cxe je treba, bo kvaliteta zmanjsxana. Cxe je 'r'
  % zelo majhen, ni garantirano, da bo slika res tako dobro stisnjena, ker to
  % ni vedno mogocxe.
  % 'hitro' dolocxa hitrost stiskanja. V hitrem nacxinu (true) se bo kompresor
  % zadovoljil z velikostjo stisnjenega bloka takoj, ko bo dovolj majhna,
  % rekurzijo pa bo uporabil le, cxe SVD razcep bloka ni dal dovolj dobrega
  % rezultata. V pocxasnem nacxinu (false) bo vedno poskusil oba nacxina in
  % uporabil najboljsxega. To izboljsxa razmerje sxe za 10-15%, a traja 3-6x
  % dlje.
  %
  % Vrne vektor podatkov (v plavajocxi vejici), ki ga je mogocxe npr. shraniti
  % v datoteko ali podati funkciji Rekonstruiraj za dekompresijo.

  tic();
  v = AdaptExec(A, 1, 1, size(A, 2), size(A, 1), q, r, hitro);
  v = [size(A), v];
  printf('%d (%.2f%%), %.3f s\n', length(v), length(v) / numel(A) * 100, toc());
end

function v = AdaptExec(A, x, y, w, h, q, r, hitro)
  % Funkcija, ki izvede adaptivno stiskanje. Stisne dani podblok matrike A
  % in, cxe stisnjena verzija zavzame prevecx prostora, jo razdeli na pol po
  % krajsxi dimenziji in rekurzivno ponovi postopek v upanju, da bo to
  % porabilo manj prostora. Cxe ne, ohrani stari (vecxji) blok.
  % Vrne vektor, ki vsebuje podatke, potrebne za rekonstrukcijo bloka oz.
  % [disjunktnih] podblokov, ki ga sestavljajo.

  B = A(y : y + h - 1, x : x + w - 1);

  % Naredimo SVD razcep in shranjujemo singularne vernosti in pripadajocxe
  % vektorje, dokler ne dosezxemo zxelene kvalitete (dobro) ali presezxemo
  % zxelenega razmerja velikosti (slabo).
  [U, S, V] = svd(B);
  vsotaS = sum(sum(S));
  vsota = 0;
  v1 = [x, y, h, w, 0];  % (0 je placeholder za i)
  k = min(size(S, 1), size(S, 2));
  i = 1;
  while true
    if i > 1 && length(v1) > r * numel(B)
      ok = false;
      break;
    elseif i > k || vsotaS == 0 || vsota / vsotaS >= q
      ok = hitro;
      break;
    end
     
    v1 = [v1, U(:, i)', S(i, i), V'(i, :)];
    v1(5) = i;
    vsota = vsota + S(i, i);
    i = i + 1;
  end

  % Cxe SVD razcep ni prinesel dovolj majhnega vektorja ali pa hocxemo
  % prihraniti res vsak bajt, razdelimo blok na dva.
  if ~ok && size(B, 1) >= 3 && size(B, 2) >= 3
    if size(B, 1) < size(B, 2)
      % Razdelimo na levi in desni podblok.
      k = floor(w / 2);
      v2 = AdaptExec(A, x, y, k, h, q, r, hitro);
      v3 = AdaptExec(A, x + k, y, w - k, h, q, r, hitro);
    else
      % Razdelimo na zgornji in spodnji podblok.
      k = floor(h / 2);
      v2 = AdaptExec(A, x, y, w, k, q, r, hitro);
      v3 = AdaptExec(A, x, y + k, w, h - k, q, r, hitro);
    end
    if length(v2) + length(v3) < length(v1)
      v = [v2, v3];
    else
      v = v1;
    end
  else
    v = v1;
  end
end