function svdcolor()
  A = imread("./slike/dragic.jpg");
  k = 16; % velikost bloka
  q = 0.9; % kvaliteta - 1 najboljše, 0 najslabše
  
  kompresor = @(blok) svds_kompresor(blok, q);
  dekompresor = @svd_dekompresor;
  
  R=A(:,:,1);
  G=A(:,:,2);
  B=A(:,:,3);
  
  R=svdchannel(R, k, kompresor, dekompresor);
  G=svdchannel(G, k, kompresor, dekompresor);
  B=svdchannel(B, k, kompresor, dekompresor);
  
  N=R;
  N(:,:,2)=G;
  N(:,:,3)=B;
  
  imwrite(N,"./slike/dragick16q09.png");
end