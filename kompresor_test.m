function kompresor_test
% sluzi testiranju delovanja kompresorjev

% da dodamo mapo s kompresordi
addpath("./kompresija")

% obicanjni svd
% ker mora imeti kompresor kot argument le blok, brez (za svoje delovanje potrebnih)
% parametrov, ki dolocajo kriterij kompresije, tega ovijemo v anonimno funkcijo
% z njegovim argumentom ter to podamo funkciji za obdelavo slike
svd_k = @(blok) svd_kompresor(blok, 0.7); % faktor kompresije
grayIm = imread("./slike/testni_vzorec_400_sivine.png");
grayImK = svdchannel(grayIm, 20, svd_k, @svd_dekompresor);

% preko anonimnih funkcij lahko tvorimo tudi predprocesor za podatke pikslov,
% v sledecem primeru to uporabimo, da prvo pretvorimo vrednosti v obmocje med -1
% in 1, nato pa nad tem naredimo svd razcep
svd_racio_k = @(blok) svd_kompresor((double(blok)./255)*2-1, 0.7);
svd_racio_d = @(Uk, Sk, Vk) ((svd_dekompresor(Uk, Sk, Vk)+1)./2).*255;
grayImRacioK = svdchannel(grayIm, 20, svd_racio_k, svd_racio_d);

% naredimo side-by-side primerjavo med 1. kompresijo, 2. kompresijo ter razliko
% med njima
figure(1);

subplot(1,3,1); % 1 vrstica, 3 stolpci, 1 aktiven
imshow(grayImK);
axis("on");
grid("on");
title("svd, 20, 0.7")

subplot(1,3,2);
imshow(grayImRacioK);
axis("on");
grid("on");
title("svd [-1,1], 20, 0.7")

grayImRacioK;

subplot(1,3,3);
imshow(grayImRacioK-grayImK);
axis("on");
grid("on");
title("razlika")
end