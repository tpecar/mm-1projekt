function podatki = pridobi_podatke()
  disp("Kontaktiram streznik...");
  fflush(stdout);
  goli_podatki = urlread(...
"ftp://aftp.cmdl.noaa.gov/data/trace_gases/co2/in-situ/surface/mlo/\
co2_mlo_surface-insitu_1_ccgg_DailyData.txt");
  disp("Pridobil podatke, filtriram ter interpretiram...");
  fflush(stdout);
  % filtriramo dobljeno ter interpretiramo v matriko
  [leto, mesec, dan, ppm] = strread(regexprep(goli_podatki,...
      '(#[^#\n]*\n)|(MLO[^-]*-999.99[^\n]*\n)',''),...
      "%* %f %f %f %* %* %* %f %* %* %* %* %* %* %* %* %*");
  % ker je nas model vezan na leta, bomo dneve ter mesece preoblikovali v
  % decimalni del leta
  % uporabimo datenum(leto,mesec,dan)-datenum(leto,1,1)
  % ter delimo s stevilom dnevov v letu, pri cemer z is_leap_year ugotovimo ali
  % delimo s 365 ali 366
  disp("Pretvarjam casovne enote...");
  fflush(stdout);
  prvi = ones(length(leto),1);
  podatki = [leto + (datenum(leto,mesec,dan)-(datenum(leto,prvi,prvi)))...
            ./(365+is_leap_year(leto)), ppm];
  disp("Koncano.");
  fflush(stdout);
  % dobljene podatke lahko prikazemo z
  % plot(podatki(:,1),podatki(:,2))
endfunction