function naloge(pod)
  % Resevanje 1. domace naloge MM s pomocjo modela
  % Tadej Pecar, 2015/2016
  %
  % Kot vhod pridobimo vhodne podatke [leta.dan_in_mesec_kot_decimalka ppm]
  disp(" === 1. Domaca naloga MM, Tadej Pecar, 2015/2016 ===\n")
  
  % izracunamo parametre co2 modela
  p = parametri(pod(:,1),pod(:,2),@co2,5)

  % graf za primerjavo osnovnih podatkov z izracunanim modelom, glede na
  % neodvisne spr. vzorcnih tock
  co2mod = co2(p,pod(:,1)); % izracunan co2 model
  
  figure(1);

  plot(pod(1:4:end,1),pod(1:4:end,2),'b');
  hold on;
  plot(pod(1:4:end,1),co2mod(1:4:end),'r');
  legend('izmerjeni podatki','model','location','northwest');
  xlabel('leto');
  ylabel('ppm CO2');
  title("Model v primerjavi z dejanskimi podatki");
  grid('on')
  % 1. naloga
  % Kaksno povprecno koncentracijo napove model za leto 2030 in 2050?
  disp("---------------------------------------------------------------------");
  disp("1. naloga")
  function naloga1(leto)
    % zgeneriramo decimalni delez leta za x
    x = 0:1./365:1;
    printf("Povprecna koncentracija za leto %d je %6.3f ppm\n", ...
           leto, (ones(1,length(x)) *co2(p, [x+leto]'))./length(x));
  endfunction
  naloga1(2030);
  naloga1(2050);
  
  % 2. naloga
  % Koliko je amplituda letnega nihanja koncentracije?
  disp("---------------------------------------------------------------------");
  disp("2. naloga");
  function naloga2
    figure(2);
    step = 0.001; % posamezen korak
    x = [0:step:1]';
    y = co2([zeros(3,1);p(4:5,:)],x);
    plot(x,y);
    % numericna izvedba je le za primerjavo z numericno izpeljavo
    [max_y, max_ind] = max(y);
    [min_y, min_ind] = min(y);
    % izracun po izpeljavi
    ampl = p(4)./(sin(pi/2+acot(p(4)./p(5))));
    printf("Minimum %12.10f, Maksimum %12.10f\n\
Dejanska amplituda %12.10f\n\
Razlika %12.10f\n", ...
      min_y,max_y,ampl,abs(max(abs(min_y),abs(max_y))-ampl));
    hold on;
    plot(x(max_ind),max_y,'r*');
    text(x(max_ind)+step*100,max_y,sprintf('MAX %f',max_y));
    plot(x(min_ind),min_y,'r*');
    text(x(min_ind)+step*100,min_y,sprintf('MIN %f',min_y));
    title("Periodicni del modela");
    grid('on')
  endfunction
  naloga2;
  
  % 3. naloga
  % Za koliko se vsako leto v povprecju poveca letni prirastek koncentracije?
  % POTREBNO JE PAZITI, da naloga od tebe ne zahteva povprecnega prirastka,
  % temvec povprecno spremembo prirastka
  
  % Povprecja bomo dobili na 2 nacine:
  % - preko povprecenja dejanskih podatkov  - to zal moramo
  %  delati rocno zaradi razlicnega stevila podatkov za posamezno leto
  % - preko povprecenja modela, za primerjavo s podatki
  % - preko modela, kjer zanemarimo periodicni del modela
  % kljub poenostavitvi bi morali dovolj blizu realnim podatkom
  disp("---------------------------------------------------------------------");
  disp("3. naloga");
  function [avg_x, podpr] = prirastki(inpod)
    leto = fix(inpod(1:1));
    
    sum = 0; % trenutni sestevek, iz katerega izracunamo povrecje
    zind = 1; % zacetni indeks tega leta - iz tega pridobimo stevilo elementov
    aind = 1; % indeks v tabeli povprecij
    
    % za preverjanje vhoda
    pleto = 0;
    pnum = 0;
    
    for i=1:length(inpod)
      if(inpod(i,1)-leto>=1) % ce smo v naslednjem letu, izracunamo povprecje
                          % prejsnjega ter ponastavimo stevce
        %printf("za leto %d zbranih %d podatkov\n",leto,i-zind);
        leto = fix(inpod(i,1));
        avg_l(aind) = sum./(i-zind); % shranimo povprecje ppm leta
        sum = 0;
        zind = i;
        aind = aind + 1;
      end
      sum = sum + inpod(i,2);
      if(inpod(i,1)<pleto)
        printf("POZOR: [%d] t.leto [%12.8f] < p.leto [%12.8f]\n",pnum,inpod(i,1),pleto);
        pnum = pnum + 1
      end
      pleto=inpod(i,1);
    end
    % zadnjega leta ne upostevamo, saj ta povecini ne predstavlja realne vrednosti
    
    % izracunamo razlike - dejanske prirastke, prvi ter zadnji clen razlike
    % odstranimo ker nismo imeli podatkov
    podpr = ([avg_l,0]-[0,avg_l])(2:end-1);
    
    % dolocimo pripadajoca leta povprecjem, pri cemer se drzimo predpostavke, da
    % je povprecje nekega leta razlika med predhodnim ter trenutnim - to pomeni
    % da za prvo leto nimamo povprecja, zacnemo sele z naslednjim
    avg_x = fix(inpod(1,1)+1):1:fix(leto-1);
  endfunction
  function naloga3
    % dolocimo prirastke dejanskih podatkov
    [podpr_x, podpr_y] = prirastki(pod);
    
    % izracunamo modela brez periodicnega dela (polinomski del)
    polp = [p(1:3);0;0];
    % tezava dane metode dolocanja prirastkov je, da mi imamo razlicno stevilo
    % podatkov za posamezna leta - posamezni datumi manjkajo, kar pomeni da
    % povprecje nekoliko drugacno kot pa bi bilo direktno po funkciji
    % temu se lahko izognemo, da zgeneriramo interval datumov
    podmod = [fix(pod(1,1)):(1./365):fix(pod(end,1))+1]';
    [modpr_x modpr_y] = prirastki([podmod co2(p, podmod)]);
    [polpr_x polpr_y] = prirastki([podmod co2(polp, podmod)]);
    
    % izris prirastkov
    figure(3)
    plot(podpr_x, podpr_y, '-bo');
    hold on
    plot(modpr_x, modpr_y, '-rx');
    plot(polpr_x, polpr_y, '-go');
    grid('minor','on')
    legend('podatki','periodicni model','polinomski model', 'location', 'northwest');
    xlabel('leto');
    ylabel('prirastek ppm CO2');
    title("Povprecni letni prirastek");
    grid('on')
    
    % dolocimo razlike prirastkov ter povprecimo se to
    podpr_dif_y = ([podpr_y,0]-[0,podpr_y])(2:end-1);
    modpr_dif_y = ([modpr_y,0]-[0,modpr_y])(2:end-1);
    polpr_dif_y = ([polpr_y,0]-[0,polpr_y])(2:end-1);
    
    figure(4)
    % zaradi razlik imamo sedaj en podatek za leto manj
    % privzemimo dogovor, da prirastek pomeni spremembo od prejsjega leta do
    % trenutnega, zaradi cesar zacnemo z drugim letom
    plot(podpr_x(2:end), podpr_dif_y, '-bo');
    hold on
    plot(modpr_x(2:end), modpr_dif_y, '-rx');
    plot(polpr_x(2:end), polpr_dif_y, '-go');
    grid('minor','on')
    legend('podatki','periodicni model','polinomski model', 'location', 'northwest');
    xlabel('leto');
    ylabel('sprememba prirastka ppm CO2');
    title("Povprecna sprememba letnih prirastkov");
    grid('on')
    disp("Primerjava letnih prirastkov podatkov z modelom brez periodicnega dela:")
    disp  ("Povprecni letni prirastek:")
    printf("  - po podatkih:              %5.4f ppm/leto\n", ...
              (podpr_dif_y*ones(length(podpr_dif_y),1))./length(podpr_dif_y));
    printf("  - po modelu z oscilacijami: %5.4f ppm/leto\n", ...
              (modpr_dif_y*ones(length(modpr_dif_y),1))./length(modpr_dif_y));
    printf("  - po polinomskem modelu:    %5.4f ppm/leto\n", ...
              (polpr_dif_y*ones(length(polpr_dif_y),1))./length(polpr_dif_y));
  endfunction
  naloga3
  disp("---------------------------------------------------------------------");
endfunction