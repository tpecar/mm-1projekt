function y = co2(p,x)
  % p je vektor parametrov, x je datum predstavljen kot 
  % leto.[dan, mesec kot delez enega leta]
  y = p(1)+p(2)*x+p(3)*x.^2+p(4)*sin(2*pi*x)+p(5)*cos(2*pi*x);
endfunction