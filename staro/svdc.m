function svdc()
  A = imread("./slike/dg.png");
  k = 32; % velikost bloka
  q = 0.2; % kvaliteta - 1 najboljše, 0 najslabše
  
  for vrstica=1:k:size(A,1)
    for stolpec=1:k:size(A,2)
      % blok ki ga trenutno obdelujemo
      M = A(vrstica:(vrstica+k-1), stolpec:(stolpec+k-1));
      [U, S, V] = svd(M);
      sumAll = sum(S);
      sumSelect = 0;
      
      idx = 1; % indeks v diagonali
      while(sumSelect./sumAll<=q && idx < k)
        sumSelect += S(idx,idx);
        idx++;
      end
      % v matrike singualrnih vrednosti, ki je velika kot A
      % shranimo U, S in V, pri cemer shranimo le do idx
      
      % ZA PROBO, izracunamo M ter shranimo nazaj v A
      M = U(:,1:idx)*S(1:idx,1:idx)*V(:,1:idx)';
      A(vrstica:(vrstica+k-1), stolpec:(stolpec+k-1)) = M;
    end
  end
  
  imwrite(A,"dcomp.png");
end